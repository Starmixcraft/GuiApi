package de.starmixcraft.guiapi.gui;

import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;


import java.util.ArrayList;
import java.util.List;

public class ItemStackBuilder {

    private final ItemStack ITEM_STACK;

    public ItemStackBuilder(Material mat) {
        this.ITEM_STACK = new ItemStack(mat);
    }
    
    public ItemStackBuilder(ItemStack item) {
        this.ITEM_STACK = item;
    }
    /**
     * Will set the amount of the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder withAmount(int amount) {
        ITEM_STACK.setAmount(amount);
        return this;
    }
    /**
     * Will set the name of the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder withName(String name) {
        final ItemMeta meta = ITEM_STACK.getItemMeta();
        meta.setDisplayName(name);
        ITEM_STACK.setItemMeta(meta);
        return this;
    }
    /**
     * Will set the lore of the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder withLore(String... lore) {
        for (String s : lore) {
            withLore(s);
        }

        return this;
    }
    /**
     * Will set the lore of the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder withLore(String name) {
        final ItemMeta meta = ITEM_STACK.getItemMeta();
        List<String> lore = meta.getLore();
        if (lore == null) {
            lore = new ArrayList<>();
        }
        lore.add(name);
        meta.setLore(lore);
        ITEM_STACK.setItemMeta(meta);
        return this;
    }
    /**
     * Will set the Durability of the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder withDurability(int durability) {
        ITEM_STACK.setDurability((short) durability);
        return this;
    }
    /**
     * Will set the Data of the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder withData(int data) {
        ITEM_STACK.setDurability((short) data);
        return this;
    }
    /**
     * Will add a Enchantment with level to the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder withEnchantment(Enchantment enchantment, final int level) {
        ITEM_STACK.addUnsafeEnchantment(enchantment, level);
        return this;
    }
    /**
     * Will add a Enchantment to the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder withEnchantment(Enchantment enchantment) {
        ITEM_STACK.addUnsafeEnchantment(enchantment, 1);
        return this;
    }
    /**
     * Will set the type of the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder withType(Material material) {
        ITEM_STACK.setType(material);
        return this;
    }
    /**
     * Will clear the lore from the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder clearLore() {
        final ItemMeta meta = ITEM_STACK.getItemMeta();
        meta.setLore(new ArrayList<String>());
        ITEM_STACK.setItemMeta(meta);
        return this;
    }
    /**
     * Will clear all Enchantments from the item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder clearEnchantments() {
        for (Enchantment enchantment : ITEM_STACK.getEnchantments().keySet()) {
            ITEM_STACK.removeEnchantment(enchantment);
        }
        return this;
    }
    /**
     * 
     * @param color
     * @return This {@link ItemStackBuilder}
     * @throws Exception {@link UnsupportedOperationException} if items has no color tag
     */
    public ItemStackBuilder withColor(Color color) throws UnsupportedOperationException{
    	if(color == null) {
    		return this;
    	}
        Material type = ITEM_STACK.getType();
        if (type == Material.LEATHER_BOOTS || type == Material.LEATHER_CHESTPLATE || type == Material.LEATHER_HELMET || type == Material.LEATHER_LEGGINGS) {
            LeatherArmorMeta meta = (LeatherArmorMeta) ITEM_STACK.getItemMeta();
            meta.setColor(color);
            ITEM_STACK.setItemMeta(meta);
            return this;
        } else {
            throw new UnsupportedOperationException("withColor is only applicable for leather armor!");
        }
    }

    /**
     * @return The {@link ItemStack}
     */
    public ItemStack build() {
        return ITEM_STACK;
    }

    /**
     * Will hide all Enchantments from an item<br>
     * @return This {@link ItemStackBuilder}
     */
    public ItemStackBuilder hideFlags() {
        ItemMeta meta = ITEM_STACK.getItemMeta();
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        return this;
    }
    /**
     * Will Set the Owner of a Skull<br>
     * @return This {@link ItemStackBuilder}
     * @throws Exception {@link UnsupportedOperationException} if items is not a Skull
     */
    @SuppressWarnings("deprecation")
	public ItemStackBuilder setskullowner(String name) throws UnsupportedOperationException{
    	if(ITEM_STACK.getType() == Material.SKULL_ITEM) {
    		SkullMeta sm = (SkullMeta) ITEM_STACK.getItemMeta();
    		sm.setOwner(name);
    		ITEM_STACK.setItemMeta(sm);
    		return this;
    	}else {
    		throw new UnsupportedOperationException("setskullowner is only applicable for SKULL_ITEM!");
    	}
    }
}