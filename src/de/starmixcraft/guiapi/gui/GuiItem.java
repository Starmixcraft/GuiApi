package de.starmixcraft.guiapi.gui;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.function.BiConsumer;

public class GuiItem {

    private BiConsumer<Player, ItemStack> consumer;
    private ItemStack itemStack;

    public GuiItem(BiConsumer<Player, ItemStack> consumer, ItemStack itemStack) {
        this.consumer = consumer;
        this.itemStack = itemStack;
    }

    public BiConsumer<Player, ItemStack> getConsumer() {
        return consumer;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }
}
