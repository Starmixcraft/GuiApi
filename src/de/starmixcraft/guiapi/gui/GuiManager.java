package de.starmixcraft.guiapi.gui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;


public class GuiManager implements Listener{

    public static GuiManager instance;
    private List<GuiBuilder> builders;

    public GuiManager(JavaPlugin plugin) {
        instance = this;
        Bukkit.getPluginManager().registerEvents(this, plugin);
        this.builders = new ArrayList<>();
    }

    public void add(GuiBuilder builder) {
        this.builders.add(builder);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        if (event.getCurrentItem() == null) {
            return;
        }
        if (!event.getCurrentItem().hasItemMeta()) {
            return;
        }

        if (!event.getCurrentItem().getItemMeta().hasDisplayName()) {
            return;
        }

        for (GuiBuilder builder : builders) {
            if (builder.getInventory().getTitle().equalsIgnoreCase(event.getInventory().getTitle())) {
                for (GuiItem item : builder.getItems()) {
                    if (item.getItemStack().equals(event.getCurrentItem())) {
                        event.setCancelled(true);
                        if (item.getConsumer() != null) {
                            item.getConsumer().accept((Player) event.getWhoClicked(), event.getCurrentItem());
                        }
                        break;
                    }
                }
                break;
            }
        }
    }

    public void remove(GuiBuilder builder) {
        this.builders.remove(builder);
    }
}
