package de.starmixcraft.guiapi.gui;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

public class GuiBuilder {

    private Inventory inventory;
    private List<GuiItem> items;

    public GuiBuilder(String title, int slots) {
        inventory = Bukkit.createInventory(null, slots, title);
        items = new ArrayList<>();
    }

    public GuiBuilder withItem(ItemStack itemStack, int slot, BiConsumer<Player, ItemStack> consumer) {
        inventory.setItem(slot, itemStack);
        items.add(new GuiItem(consumer, itemStack));
        return this;
    }

    public GuiBuilder withItem(ItemStack itemStack, BiConsumer<Player, ItemStack> consumer) {
        inventory.addItem(itemStack);
        items.add(new GuiItem(consumer, itemStack));
        return this;
    }

    public GuiBuilder open(Player player) {
        player.openInventory(inventory);
        return this;
    }

    public GuiBuilder save() {
        GuiManager.instance.add(this);
        return this;
    }

    public GuiBuilder remove() {
        GuiManager.instance.remove(this);
        return this;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public List<GuiItem> getItems() {
        return items;
    }

    public GuiBuilder clear() {
        inventory.clear();
        return this;
    }

    public GuiBuilder updateItem(int slot, ItemStack itemStack, BiConsumer<Player, ItemStack> consumer) {
        withItem(itemStack, slot, consumer);
        remove().save();
        return this;
    }
}
