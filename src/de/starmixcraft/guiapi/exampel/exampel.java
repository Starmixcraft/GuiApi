package de.starmixcraft.guiapi.exampel;

import java.util.function.BiConsumer;

import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import de.starmixcraft.guiapi.gui.GuiBuilder;
import de.starmixcraft.guiapi.gui.ItemStackBuilder;

public class exampel implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		Player p = (Player) sender;
		//Create a gui builder
		GuiBuilder builder = new GuiBuilder("exampel Gui", 27);
		
		//Add an item to the builder
		builder.withItem(
				//Create itemstackbuilder
				new ItemStackBuilder(Material.TNT).withName("§2TNT").withAmount(4).withEnchantment(Enchantment.ARROW_FIRE).hideFlags().withLore("Lore").build(), 
				0, new BiConsumer<Player, ItemStack>() {
					
					@Override
					public void accept(Player t, ItemStack u) {
						//Called wen the player klicks the item
						
					}
				});
		
		
		
		//important allways save the builder for the listern to check klick events!!
		builder.save();
		
		//Open the gui for the player
		builder.open(p);
		
		
		return false;
	}

}
